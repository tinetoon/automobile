package ru.kommersant.AppManager;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

import static java.util.concurrent.TimeUnit.SECONDS;

public class ApplicationManager {

    protected MobileDriver driver;

    private NavigationHelper navigationHelper;
    private String browserName;
    private DesiredCapabilities capabilities;

    public ApplicationManager(String browserName) {

        this.browserName = browserName;
    }

    public void init(String url) throws MalformedURLException {

        if (browserName.equals("android")) {

            capabilities = new DesiredCapabilities();
            capabilities.setCapability("appium:deviceName", "Pixel");
            capabilities.setCapability("appium:", "emulator-5554");
            capabilities.setCapability("platformName", "Android");
            capabilities.setCapability("appium:platformVersion", "11");
            capabilities.setCapability("appium:app", "c:/Users/Anton/myProgram/Kommersant_4.3_apkcombo.com.apk"); //C:/Java/automobile
            capabilities.setCapability("appium:automationName", "UiAutomator2");

            driver = new AndroidDriver(new URL(url), capabilities);
        }

        // Implicit Wait (Неявное ожидание) - выставляем 10 секунд (!!! обязательно определяем в коде)
        driver.manage().timeouts().implicitlyWait(10, SECONDS);

        navigationHelper = new NavigationHelper(driver);
    }

    // Метод завершения тестов
    public void stop() {
        driver.quit();
    }

    public NavigationHelper getNavigationHelper() {
        return navigationHelper;
    }
}
