package ru.kommersant.AppManager;

import io.appium.java_client.MobileDriver;

/**
 * Класс помощник навигации приложению
 * Ответственный за создание класса - А.А. Дюжаков
 * Дата создания - 2022-04-24
 */

public class NavigationHelper extends HelperBase {

    public NavigationHelper(MobileDriver driver) {
        super(driver);
    }

    // Метод закрывающий рекламный баннер
    public void closeBanner() {
        click(lc.getCLOSE_BANNER()); // !!! Добавить проверку на существование баннера
    }

    // Метод перехода на страницу профиля
    public void goToProfilePage() {
        click(lc.getMENU_PAGE_PROFILE());
    }

    // Метод входа в учётную запись
    public void goToLoginPage() {
        click(lc.getBUTTON_ENTRY_A_ID());
    }

    // Метод авторизации
    public void authorization(String email, String password) {
        fillingField(lc.getFILL_LOGIN_EMAIL(), email);
        fillingField(lc.getFILL_PASSWORD(), password);
        click(lc.getBUTTON_ENTRY_TO_ACCOUNT());
    }

    // Метод проверки ошибки логина
    public boolean isErrorEmailLogin() {
        return isElementPresent(lc.getERROR_EMAIL_LOGIN_TEXT());
    }

    // Метод проверки ошибки пароля
    public boolean isErrorPassword() {
        return isElementPresent(lc.getERROR_PASSWORD_TEXT());
    }
}
