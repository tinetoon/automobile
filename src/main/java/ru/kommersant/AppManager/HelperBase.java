package ru.kommersant.AppManager;

import io.appium.java_client.MobileDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import ru.kommersant.TestData.Locators;

/**
 * Базовый класс помощников
 * Ответственный за создание класса - А.А. Дюжаков
 * Дата создания - 2022-04-24
 */

public class HelperBase {

    protected static Locators lc = new Locators();
    protected MobileDriver md;

    public HelperBase(MobileDriver driver) {
        this.md = driver;
    }

    protected void click(By locator) {
        md.findElement(locator).click();
    }

    protected void click(String accessibilityId) {
        md.findElementByAccessibilityId(accessibilityId).click();
    }

    // Метод, заполняющий текстовое поле ввода
    protected void fillingField(By locator, String text) {
        md.findElement(locator).click();
        md.findElement(locator).clear();
        md.findElement(locator).sendKeys(text);
        /*if (text != null) {
            String existingText = md.findElement(locator).getAttribute("value");
            if (! text.equals(existingText)) {
                md.findElement(locator).clear();
                md.findElement(locator).sendKeys(text);
            }
        }*/
    }

    // Метод, проверяющий наличие элемента на странице
    protected boolean isElementPresent(By locator) {

        try {
            md.findElement(locator);
            return true;

        } catch (NoSuchElementException e) {
            return false;
        }
    }
}
