package ru.kommersant;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.kommersant.model.TestBase;

/**
 * Класс для тестирования авторизации в мобильном приложении kommersant
 * Ответственный за создание класса - А.А. Дюжаков
 * Дата создания - 2022-04-24
 */

public class AuthorizationTest extends TestBase {

    @Test(description = "Негативная проверка авторизации (пустые поля)")
    void NegativeAuthorizationCheck() {

        // Закрываем рекламный баннер
        app.getNavigationHelper().closeBanner();

        // Переходим на страницу авторизации
        app.getNavigationHelper().goToProfilePage();

        // Открываем форму авторизации
        app.getNavigationHelper().goToLoginPage();

        // Нажимаем кнопку Вход
        app.getNavigationHelper().authorization("", "");

        // Получаем сообщение об ошибке
        Assert.assertTrue(app.getNavigationHelper().isErrorEmailLogin());
        Assert.assertTrue(app.getNavigationHelper().isErrorEmailLogin());

    }
}
