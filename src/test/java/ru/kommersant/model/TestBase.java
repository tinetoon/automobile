package ru.kommersant.model;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import ru.kommersant.AppManager.ApplicationManager;
import ru.kommersant.TestData.Locators;

import java.net.MalformedURLException;

/**
 * Базовый класс для тестирования приложения kommersant
 * Ответственный за создание класса - А.А. Дюжаков
 * Дата создания - 2022-04-24
 */

public class TestBase {

    protected static Locators lc = new Locators();
    private static String TYPE_OS = "android";
    private static String URL = "http://127.0.0.1:4723/wd/hub";

    protected static final ApplicationManager app = new ApplicationManager(TYPE_OS);

    @BeforeSuite
    public void setUp() {
        try {
            app.init(URL);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e); // !!! разобраться с обработкой исключений
        }
    }

    @AfterSuite
    public void tearDown() {
        app.stop();
    }
}
